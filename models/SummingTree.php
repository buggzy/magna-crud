<?php

namespace app\models;
use Yii;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;

class SummingTree extends ActiveRecord {
	
	public function rules(){
		return [
			[['title', 'value'], 'safe'],
			['value', 'number']
		];
	}
	
	public function attributeLabels(){
		return [
			'title' => 'Строковое значение',
			'value' => 'Числовое значение'
		];
	}
	
    public static function tableName()
    {
        return 'summing_tree';
    }
    
    public static function findByPk($id){
    	return self::findOne(['id'=>$id]);
    }
    
    public static function getProvider($parent){
    	
    	$query = self::find()->where(['parent_id'=>$parent]);
    	
    	$provider = new ActiveDataProvider([
		    'query' => $query,
		    'pagination' => [
		        'pageSize' => 20,
		    ],
		]);
		
		return $provider;
    	
    }

    public static function sumabove($id){
    	$retval = 0;
    	$item = self::findByPk($id);
    	
    	while($item){
    		$retval += $item->value;
    		$item = self::findByPk($item->parent_id);
    	}
    	return $retval;
    }

    
    public static function sumbelow($parent){
    	$retval = 0;
    	// не буду париться с левыми и правыми, тупо рекурсия
    	$list = self::findAll(['parent_id'=>$parent]);
    	foreach($list as $item){
    		if($item->value)
    			$retval += $item->value;
    			
    		$retval += self::sumbelow($item->id);
    		
    	}
    	return $retval;
    }

    public static function sumabove_string($id){
    	$retval = '';
    	$item = self::findByPk($id);
    	
    	while($item){
    		$retval = $item->value . $retval;
    		$item = self::findByPk($item->parent_id);
    	}
    	return $retval;
    }

    
    public static function sumbelow_string($parent){
    	$retval = '';
    	// не буду париться с левыми и правыми, тупо рекурсия
    	$list = self::findAll(['parent_id'=>$parent]);
    	foreach($list as $item){
    		if(null == $item->value)
    			$retval .= '(null)';
		else
    			$retval .= $item->value;
    			
    		$retval .= self::sumbelow($item->id);
    		
    	}
    	return $retval;
    }
}
