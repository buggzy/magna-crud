Как тестировать
===============
Рабочая версия - здесь

http://magna-crud.picvo.ru/
Само задание - в пункте меню CRUD, он закрыт под паролем, пароль написан на странице ввода пароля. Разделение доступов чтение\запись не стал делать.

* Плюсик - добавить элемент
* Иконка редактировать - редактируе
* Стрелочка вверх, стрелочка вниз - на уровень выше или ниже дерева
* Крестик - удалить
* По идее, там есть пагинация, но я ее не тестировал, доверяю фреймворку что она будет
* Элементы поименованы (в задании не было, но без этого грустно)
* Суммы подписаны

Весь код - в controllers/CrudController и models/SummingTree, остальное в основном заводское, что не заводское - видно в первом коммите.

Если хочется поставить это себе: установить фреймворк Yii2 basic (там ниже англоязычная инструкция), залить базу из дампа (в папке sql), прописать логины и пароль к ней в config/db

Подробную документацию в рамках неоплачиваемого тестового задания писать лень.


Тестовое задание
=================

Написать PHP приложение ( сайт, страницу ) для создания и редактирования дерева данных. 
У любого узла может быть любое количество дочерних узлов.
У каждого дочернего узла может быть только один родительский.
Каждый узел хранит строку, которая может быть пустой ( NULL ). 
Для каждого узла можно рассчитать сумму его дочерних и сумму его родительских.
Данные должны храниться в базе под управлением СУБД MySQL 5.

Выполненное задание должно содержать :
1) исходники веб-страниц и базы ;
2) инструкцию по инсталляции приложения ;
3) инструкцию по работе с приложением ;
4) контрольный пример ( пошаговая инструкция ) использования приложения.

Документация должна быть в форматах *.txt или *.pdf. 

Исходники можно выложить на Git .
Можно опубликовать приложение на любом публичном хостинге.

Yii 2 Basic Project Template
============================

Yii 2 Basic Project Template is a skeleton [Yii 2](http://www.yiiframework.com/) application best for
rapidly creating small projects.

The template contains the basic features including user login/logout and a contact page.
It includes all commonly used configurations that would allow you to focus on adding new
features to your application.

[![Latest Stable Version](https://poser.pugx.org/yiisoft/yii2-app-basic/v/stable.png)](https://packagist.org/packages/yiisoft/yii2-app-basic)
[![Total Downloads](https://poser.pugx.org/yiisoft/yii2-app-basic/downloads.png)](https://packagist.org/packages/yiisoft/yii2-app-basic)
[![Build Status](https://travis-ci.org/yiisoft/yii2-app-basic.svg?branch=master)](https://travis-ci.org/yiisoft/yii2-app-basic)

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.4.0.


INSTALLATION
------------

### Install from an Archive File

Extract the archive file downloaded from [yiiframework.com](http://www.yiiframework.com/download/) to
a directory named `basic` that is directly under the Web root.

Set cookie validation key in `config/web.php` file to some random secret string:

```php
'request' => [
    // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
    'cookieValidationKey' => '<secret random string goes here>',
],
```

You can then access the application through the following URL:

~~~
http://localhost/basic/web/
~~~


### Install via Composer

If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

You can then install this project template using the following command:

~~~
php composer.phar global require "fxp/composer-asset-plugin:~1.1.1"
php composer.phar create-project --prefer-dist --stability=dev yiisoft/yii2-app-basic basic
~~~

Now you should be able to access the application through the following URL, assuming `basic` is the directory
directly under the Web root.

~~~
http://localhost/basic/web/
~~~


CONFIGURATION
-------------

### Database

Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2basic',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

**NOTES:**
- Yii won't create the database for you, this has to be done manually before you can access it.
- Check and edit the other files in the `config/` directory to customize your application as required.
- Refer to the README in the `tests` directory for information specific to basic application tests.
