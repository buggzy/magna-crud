<?
namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\User;
use app\models\SummingTree;


class CrudController extends Controller {
	
	public function actionIndex($parent = 0){
		User::requireLoggedIn();
		
		$provider = SummingTree::getProvider($parent);

		Yii::$app->user->setReturnUrl(Yii::$app->request->getUrl());
		
		$model = SummingTree::findByPk($parent);
		$grand = $model ? $model->parent_id : false;

		return $this->render('list', ['dataProvider'=>$provider, 'parent'=>$parent, 'grand'=>$grand]);
		
	}
	
	public function actionView($id, $parent = 0){

		User::requireLoggedIn();

		if('new' == $id){
			$model = new SummingTree();
			$model->parent_id = $parent;
		} else {
			$model = SummingTree::findByPk($id);
		}
		if(!$model)
			throw new \yii\web\NotFoundHttpException;
			
		if($model->load(Yii::$app->request->post())){
			$model->save();
			$this->goBack();
		}
			
			return $this->render('element', ['model'=>$model]);
	}

	public function actionRemove($id){
		User::requireLoggedIn();

		$model = SummingTree::findByPk($id);

		if(!$model)
			throw new \yii\web\NotFoundHttpException;

		$model->delete();
		
		$this->goBack();
		
	}
	
}

?>