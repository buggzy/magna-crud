<?
use yii\grid\GridView;
use yii\helpers\Url;
use kartik\icons\Icon;
use app\models\SummingTree;
$this->title = 'crud';
?>

<a href="<? echo Url::toRoute(['crud/view', 'parent'=>$parent, 'id'=>'new'])?>"><i class="fa fa-plus"></i></a>
<? if(false !== $grand) { ?>
<a href="<? echo Url::toRoute(['crud/index', 'parent'=>$grand])?>"><i class="fa fa-arrow-up"></i></a>
<? } ?>

<div class="sum">
	Сумма того, что выше: <? echo SummingTree::sumabove_string($parent) ?>
</div>


<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'id',
        'title',
        'value',
        [
        	'content' => function ($model){
        		return SummingTree::sumbelow_string($model->id);
        	},
        	'label' => 'Сумма того, что ниже'
    	],
        [
        	'content' => function ($model){
        		return ''.
        			'<a href="'.Url::toRoute(['crud/view', 'id'=>$model->id]).'"><i class="fa fa-edit"></i></a> '.
        			'<a href="'.Url::toRoute(['crud/index', 'parent'=>$model->id]).'"><i class="fa fa-arrow-down"></i></a> '.
        			'<a href="'.Url::toRoute(['crud/remove', 'id'=>$model->id]).'"><i class="fa fa-close"></i></a>';
        	},
        	'label' => 'Действия'
        ]
    ],
]) ?>